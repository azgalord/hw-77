const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const router = express.Router();
const fs = require('fs');
const config = require('../config');

const fileName = './db.json';
let data = [];

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', (req, res) => {
  const data = fs.readFileSync(fileName);
  res.send(JSON.parse(data));
});

router.post('/', upload.single('image'), (req, res) => {
  if (req.body.message) {
    const message = req.body;
    message.id = nanoid();

    if (!message.author) {
      message.author = 'Anonymous';
    }

    try {
      const fileContents = fs.readFileSync(fileName);
      data = JSON.parse(fileContents);
    } catch (e) {
      data = [];
    }


    if (req.file) {
      message.image = req.file.filename;
    }

    data.push(message);
    fs.writeFileSync(fileName, JSON.stringify(data, null, 2));

    res.send({message: 'OK'});
  }
});

module.exports = router;