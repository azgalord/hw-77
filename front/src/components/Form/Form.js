import React from 'react';

import './Form.css';

const Form = props => {
  return (
    <div className="Form">
      <form onSubmit="">
        <input
          value={props.author} onChange={props.change}
          type="text" name="author" placeholder="Author"/>
        <input
          value={props.message} onChange={props.change}
          required
          type="text" name="message" placeholder="Message"/>
        <div style={{display: 'flex', alignItems: 'center'}}>
          <label htmlFor="image">Image</label>
          <input
            onChange={props.fileChange}
            type="file" name="image" id="image"/>
        </div>
        <button type="submit" onClick={props.submit}>Submit</button>
      </form>
    </div>
  );
};

export default Form;