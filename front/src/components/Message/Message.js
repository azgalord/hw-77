import React from 'react';

import './Message.css';
import {apiURL} from "../../constants";

const Message = props => {
  return (
    <div className="Message">
      <span><b>Author : </b> {props.author}</span>
      <div className="MessageContent">
        {props.image ? <img src={apiURL + '/uploads/' + props.image} alt=""/> : null}
        <p>{props.message}</p>
      </div>
    </div>
  );
};

export default Message;