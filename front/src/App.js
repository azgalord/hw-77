import React, { Component } from 'react';
import './App.css';
import {connect} from "react-redux";
import {fetchMessages, sendMessageToServer} from "./store/messagesActions";
import Message from "./components/Message/Message";
import Form from "./components/Form/Form";

class App extends Component {
  state = {
    author: '',
    message: '',
    image: '',
  };

  componentDidMount() {
    this.props.fetchMessages();
  }

  changeHandler = event => {
    this.setState({[event.target.name] : event.target.value});
  };

  fileChangeHandler = event => {
    this.setState({[event.target.name] : event.target.files[0]});
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.sendMessageToServer(formData);
  };

  render() {
    if (!this.props.messages) {
      return <div>Loading...</div>
    }
    return (
      <div className="App">
        <div className="messages">
          {this.props.messages.map(message => (
            <Message
              author={message.author}
              message={message.message}
              key={message.id}
              image={message.image ? message.image : null}
            />
          ))}
        </div>
        <Form
          author={this.state.author}
          message={this.state.message}
          change={event => this.changeHandler(event)}
          fileChange={event => this.fileChangeHandler(event)}
          submit={event => this.submitFormHandler(event)}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  messages: state.messages,
});

const mapDispatchToProps = dispatch => ({
  fetchMessages: () => dispatch(fetchMessages()),
  sendMessageToServer: message => dispatch(sendMessageToServer(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
