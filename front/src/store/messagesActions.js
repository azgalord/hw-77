import axios from '../axios-messages';

export const GET_MESSAGES = 'GET_MESSAGES';

export const getMessages = response => ({type: GET_MESSAGES, response});

export const fetchMessages = () => {
  return dispatch => {
    axios.get('/messages').then(
      response => dispatch(getMessages(response.data)),
    );
  }
};

export const sendMessageToServer = message => {
  return dispatch => {
    axios.post('/messages', message).then(() => {
      dispatch(fetchMessages());
    })
  }
};