import {GET_MESSAGES} from "./messagesActions";

const initialState = {
  messages: null,
};

const messagesReducer = (state = initialState, action) => {
  switch (action.type) {
    case (GET_MESSAGES):
      return {
        ...state,
        messages: action.response
      };
    default:
      return state;
  }
};

export default messagesReducer;